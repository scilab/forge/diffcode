<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * Copyright (C) 2011 - DIGITEO - Michael Baudin
 * Copyright (C) 2002 - INRIA - Xavier Jonsson
 * Copyright (C) 2002 - INRIA - Serge Steer
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 * 
 -->

<refentry version="5.0-subset Scilab"
          xml:id="diffcode_overview"
          xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>Diffcode Overview</refname>
    <refpurpose>Diffcode Overview</refpurpose>
  </refnamediv>



  <refsection>
    <title>Description</title>
    <para>
      This toolbox enables Scilab code differentiation using operators and
      primitive functions overloading.
    </para>

    <para>
      Given a Scilab code computing a variable y depending on a variable x
      and a direction dx it allow evaluation of y together with the
      directional derivative Grad(y)*dx.
    </para>

    <para>
      It is far from complete, but supports all basic computations including
      matrix inversion.
    </para>

    <para>
      Supported operators:
      <screen>
        <![CDATA[
// Access
insertion 
extraction
// Comparison
<  
>
<= 
>= 
== 
<> 
// Arithmetic
+ 
- 
* 
.* 
\ 
.\  
/ 
./  
^ 
.^
.*.
[] 
' 
   ]]>
      </screen>
    </para>

    <para>
      Supported primitives functions:
      <screen>
// Elementary functions
abs
acos 
acosh 
asin 
asinh 
atan 
atanh 
cos 
cosh 
cotg 
exp 
log
log10 
sin 
sinh 
sqrt 
tan 
tanh 
// Conversion
string
// Matrix functions
cumsum
cumprod
conj 
det 
eye 
inv 
matrix
maxi
mini
ones 
pinv 
prod
size 
sum 
trace 
zeros
        </screen>
    </para>

    <para>
      It is easy to add new overloading functions in
      the macros directory.
    </para>
  </refsection>

  <refsection>
    <title>Quick start</title>
    <para>
      In the following example, we compute of sin(t^2) and its derivative.
      The expression depends on only one variable so the direction is set to 1.
    </para>

    <programlisting role="example">
      <![CDATA[
        t = (0:0.01:5*%pi)';
		dt = ones(t);
        y=diffcode_der(t,dt)
        y=sin(y^2);
        scf();
        plot(t,[y.v,y.dv])
   ]]>
    </programlisting>
    <para>
      It may happen that the function that we use is not 
	  defined for automatic differentiation. 
	  In this case, a message error is printed, stating that 
	  some function is undefined. 
	  In the following example, we compute the matrix-sign of 
	  a value-derivative pair. 
	  The <literal>signm</literal> function apparently internaly 
	  uses the <literal>schur</literal> function, 
	  which has not been defined for automatic differentiation.
    </para>
    <programlisting role="example">
      <![CDATA[
-->vdv = diffcode_der(0,1)
 vdv  =
TDFFCD:
=======
v:
0
dv:
1
-->signm(vdv)
 !--error 246 
Function not defined for given argument type(s),
  check arguments or define function %TDFFCD_schur for overloading.
at line      24 of function signm called by :  
signm(vdv)
   ]]>
    </programlisting>
    <para>
      In this case, all we have to do is to create the missing 
	  functions, by implementing the usual differentiation rules. 
	  This may be difficult for high level functions, but is straightforward 
	  for most common functions.
    </para>

  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>
    "The matrix cookbook", Kaare Brandt Petersen, Michael Syskind Pedersen, 2008
    </para>

    <para>
    "Matrix Differential Calculus with Applications in Statistics and Econometrics", Jan R. Magnus, Heinz Neudecker, 2007
    </para>

    <para>
    "Symbolic matrix derivatives", Paul S. Dwyer, M.S. Macphail, Ann. Math. Statist. Volume 19, Number 4 (1948), 517-534.
    </para>

  </refsection>

  <refsection>
    <title>Authors</title>
    <simplelist type="vert">
      <member>Copyright (C) 2011 - DIGITEO - Michael Baudin</member>
    </simplelist>
    <simplelist type="vert">
      <member>Copyright (C) 2002 - INRIA - Xavier Jonsson </member>
    </simplelist>
    <simplelist type="vert">
      <member>Copyright (C) 2002 - INRIA - Serge Steer</member>
    </simplelist>
  </refsection>

</refentry>
