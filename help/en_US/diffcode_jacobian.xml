<?xml version="1.0" encoding="UTF-8"?>

<!--
 *
 * This help file was generated from diffcode_jacobian.sci using help_from_sci().
 *
 -->

<refentry version="5.0-subset Scilab" xml:id="diffcode_jacobian" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">


  <refnamediv>
    <refname>diffcode_jacobian</refname><refpurpose>Compute the Jacobian of the function.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   J = diffcode_jacobian(f,x)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>f :</term>
      <listitem><para> a function or a list, the function to differenciate.</para></listitem></varlistentry>
   <varlistentry><term>x :</term>
      <listitem><para> a n-by-1 matrix of doubles, real, the point where to compute the derivatives</para></listitem></varlistentry>
   <varlistentry><term>J :</term>
      <listitem><para> a n-ny-m matrix of doubles, the exact Jacobian. The column <literal>J(:,k)</literal> approximates the gradient of <literal>fk</literal>, for <literal>k=1,2,...,m</literal>.</para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Description</title>
   <para>
Computes the exact Jacobian matrix of the function.
The algorithm  uses an exact differentiation method.
   </para>
   <para>
If <literal>f</literal> is a compiled function, then <literal>diffcode_jacobian</literal>
cannot differentiate <literal>f</literal>.
   </para>
   <para>
The function must have header
   </para>
   <para>
<screen>
y = f( x )
</screen>
   </para>
   <para>
where
<itemizedlist>
<listitem><para>x is a n-by-1 matrix of doubles,</para></listitem>
<listitem><para>y is a m-by-1 matrix of doubles.</para></listitem>
</itemizedlist>
   </para>
   <para>
It might happen that the function requires additionnal
arguments to be evaluated.
In this case, we can use the following feature.
The function <literal>f</literal> can also be the list <literal>(func,a1,a2,...)</literal>
where, <literal>func</literal>, the first element in the list,
must be a function with header
   </para>
   <para>
<screen>
y = func ( x , a1 , a2 , ... ).
</screen>
   </para>
   <para>
In this case, the input arguments <literal>a1, a2, ...</literal> will be automatically be appended at the
end of the calling sequence of <literal>func</literal>.
   </para>
   <para>
The <literal>diffcode_jacobian</literal> function calls the function
<literal>f</literal> several times in order to compute its
derivatives.
The total function evaluations is <literal>n</literal> times, where <literal>n</literal>
is the size of <literal>x</literal>.
   </para>
   <para>
</para>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// The function to differentiate
function y=f(x)
f1 = sin(x(1)*x(2))+exp(x(2)*x(3)+x(1))
f2 = sum(x.^3)
y=[f1;f2]
endfunction
// The exact gradient
function J = exactg(x)
g1(1) = cos(x(1)*x(2))*x(2)+exp(x(2)*x(3)+x(1))
g1(2) = cos(x(1)*x(2))*x(1)+exp(x(2)*x(3)+x(1))*x(3)
g1(3) = exp(x(2)*x(3)+x(1))*x(2)
//
g2(1) = 3*x(1)^2
g2(2) = 3*x(2)^2
g2(3) = 3*x(3)^2
//
J(:,1)=g1
J(:,2)=g2
endfunction
// Compute the exact Jacobian
x=[1;2;3];
J = diffcode_jacobian(f,x)
Jexact = exactg(x)
and(J==Jexact)

// Passing extra parameters
function y=G(x,p)
f1 = sin(x(1)*x(2)*p)+exp(x(2)*x(3)+x(1))
f2 = sum(x.^3)
y=[f1; f2]
endfunction
p=1;
h=1e-3;
J=diffcode_jacobian(list(G,p),x)

   ]]></programlisting>
</refsection>

<refsection>
   <title>Authors</title>
   <simplelist type="vert">
   <member>Michael Baudin, DIGITEO, 2010</member>
   </simplelist>
</refsection>
</refentry>
