// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->

// <-- Non-regression test for bug 550 -->
//
// <-- URL -->
//  http://forge.scilab.org/index.php/p/diffcode/issues/550/
//
// <-- Short Description -->
// diffcode cannot differentiate non vectorized statements.

x= [0.3;0.4];
m = 10;
for i = 1:m
  fvec(i) = exp(i*x(1))+exp(i*x(2));
end
//
v= [0.3;0.4];
dv=[1;0];
x = diffcode_der(v,dv);
for i = 1:m
  fvec(i) = exp(i*x(1))+exp(i*x(2));
end

fv = [
2.8416835
4.0476597
5.77972
8.2731493
11.870745
17.072824
24.610817
35.555707
51.477966
74.683687
];
fdv=[
1.3498588
3.6442376
7.3788093
13.280468
22.408445
36.297885
57.163189
88.185411
133.91759
200.85537
];
assert_checkalmostequal(fvec.v,fv,1.e-6);
assert_checkalmostequal(fvec.dv,fdv,1.e-6);
