// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

function y=foo(x,y)
  y=sin(x)*cos(y)
endfunction

x=1;
y=2;

d=diffcode_der([x;y],[1;-1]);

M=foo(d(1),d(2));

M.dv;
fp = [cos(x)*cos(y),-sin(x)*sin(y)]*d.dv;
assert_checkalmostequal(M.dv,fp);

