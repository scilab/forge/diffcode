// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

x=0;
xdx=diffcode_der(x,1);
A=[1 1 0.5;1 xdx xdx;0.5 0.5 0.5];
C = A';
Ev = [
1 1 0.5
1 0 0.5
0.5 0 0.5
];
Edv = [
0 0 0
0 1 0
0 1 0
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
