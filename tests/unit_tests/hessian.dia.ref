// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008-2009 - INRIA - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================
// <-- CLI SHELL MODE -->
// 1. Test with a scalar argument
function y = myfunction (x)
  y = x*x;
endfunction
x = 1.0;
expected = 2.0;
computed = diffcode_hessian(myfunction,x);
assert_checkequal ( computed , expected );
// 2. Test with a vector argument
function y = myfunction2 (x)
  y = x(1)*x(1) + x(2)+ x(1)*x(2);
endfunction
x = [1.0;2.0];
expected = [
  2 1
  1 0
  ];
//
computed = diffcode_hessian(myfunction2,x);
assert_checkequal ( computed , expected );
//
function y = myfunction3 (x,n)
  myn = 1.e4;
  y = x^(2/myn);
endfunction
x = 1.0;
myn = 1.e4;
expected = (2/myn) * (2/myn-1) * x^(2/myn-2);
computed = diffcode_hessian(myfunction3 , x );
assert_checkequal ( computed , expected );
//
// 7. Test vector output y
function y=myexample(x)
  f1 = sin(x(1)*x(2))+exp(x(2)*x(3)+x(1))
  f2 = sum(x.^3)
  y=[f1;f2]
endfunction
function H = exactH(x)
    H1(1,1) = -sin(x(1)*x(2))*x(2)^2+exp(x(2)*x(3)+x(1))
    H1(1,2) = cos(x(1)*x(2)) - sin(x(1)*x(2))*x(2)*x(1)+exp(x(2)*x(3)+x(1))*x(3)
    H1(1,3) = exp(x(2)*x(3)+x(1))*x(2)
    H1(2,1) = H1(1,2)
    H1(2,2) = -sin(x(1)*x(2))*x(1)^2+exp(x(2)*x(3)+x(1))*x(3)^2
    H1(2,3) = exp(x(2)*x(3)+x(1))+exp(x(2)*x(3)+x(1))*x(3)*x(2)
    H1(3,1) = H1(1,3)
    H1(3,2) = H1(2,3)
    H1(3,3) = exp(x(2)*x(3)+x(1))*x(2)^2
    //
    H2(1,1) = 6*x(1)
    H2(1,2) = 0
    H2(1,3) = 0
    H2(2,1) = H2(1,2)
    H2(2,2) = 6*x(2)
    H2(2,3) = 0
    H2(3,1) = H2(1,3)
    H2(3,2) = H2(2,3)
    H2(3,3) = 6*x(3)
    //
    H(:,:,1) = H1
    H(:,:,2) = H2
endfunction
x=[1;2;3];
expected = exactH(x);
computed = diffcode_hessian(myexample,x);
assert_checkequal ( computed , expected );
//
// 8. Check the number of function evaluations
function y = myFevalFun(x)
    global FEVAL
	FEVAL = FEVAL + 1
    y = sum(x.^3)
endfunction
n = 5;
x = ones(n,1);
global FEVAL;
FEVAL = 0;
H = diffcode_hessian(myFevalFun, x);
assert_checkequal ( FEVAL, n*(n+1)/2);
//
// 9. Check with extra-arguments
function y=G(x,p) 
  f1 = sin(x(1)*x(2)*p)+exp(x(2)*x(3)+x(1)) 
  f2 = sum(x.^3)
  y=[f1; f2]
endfunction
x=[1;2;3];
expected = exactH(x);
p=1;
computed=diffcode_hessian(list(G,p),x);
assert_checkequal ( computed , expected );
