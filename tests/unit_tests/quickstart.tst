// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

        t=(0:0.01:5*%pi)';
        T=diffcode_der(t,ones(t));
        y=sin(T^2);
        scf();
        plot(t,[y.v,y.dv])
