// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// In this test, we check mixed-type operations.
//
// 1. We compute the mixed type operations directly.
// 2. By promoting the double to a diffcode type, compare it
// with zero dv.
// Both computations must be equal.
// 3. We check the .v fields of the mixed operations,
// by performing the same operation directly on the .v field.

function checkOp(op,DIFFA,Bv,rtol,atol)
    Av = DIFFA.v
    Adv = DIFFA.dv
    mprintf("Checking %s\n",op);
    // 1. DIFFA OP Bv
    // Compare with DIFFA OP (Bv,zeros(Bv)
    mprintf("\t 1. DIFFA OP Bv\n");
    instr1 = "C1 = DIFFA " + op + " Bv";
    execstr(instr1);
    instr2 = "C2 = DIFFA " + op + " diffcode_der(Bv,zeros(Bv))";
    execstr(instr2);
    instr3 = "C3 = Av " + op + " Bv";
    execstr(instr3);
    assert_checkalmostequal(C1.v,C2.v,rtol,atol);
    assert_checkalmostequal(C1.dv,C2.dv,rtol,atol);
    assert_checkalmostequal(C1.v,C3,rtol,atol);
    // 2. Bv OP DIFFA
    // Compare with (Bv,zeros(Bv) OP DIFFA
    mprintf("\t 2. Bv OP DIFFA\n");
    instr1 = "C1 = Bv " + op + " DIFFA";
    execstr(instr1);
    instr2 = "C2 = diffcode_der(Bv,zeros(Bv)) " + op + " DIFFA";
    execstr(instr2);
    instr3 = "C3 = Bv " + op + " Av";
    execstr(instr3);
    assert_checkalmostequal(C1.v,C2.v,rtol,atol);
    assert_checkalmostequal(C1.dv,C2.dv,rtol,atol);
    assert_checkalmostequal(C1.v,C3,rtol,atol);
endfunction

function checkBooleanOp(op,DIFFA,Bv,rtol,atol)
    mprintf("Checking %s\n",op);
    Av = DIFFA.v
    // 1. DIFFA OP Bv
    // Compare with Av OP Bv
    mprintf("\t 1. DIFFA OP Bv\n");
    instr1 = "C1 = and(DIFFA " + op + " Bv)";
    execstr(instr1);
    instr2 = "C2 = and(Av " + op + " Bv)";
    execstr(instr2);
    assert_checkequal(C1,C2);
    // 2. Bv OP DIFFA
    // Compare with Bv OP Av
    mprintf("\t 2. Bv OP DIFFA\n");
    instr1 = "C1 = and(Bv " + op + " DIFFA)";
    execstr(instr1);
    instr2 = "C2 = and(Bv " + op + " Av)";
    execstr(instr2);
    assert_checkequal(C1,C2);
endfunction

// Define DIFFA
Av = [
  -5.    7.    3.
  -7.   -1.    2.
  -4.    3.   -7.
];
Adv = [
   -4.   6.   -7.
    2.  -6.    4.
    9.  -5.    4.
];
DIFFA=diffcode_der(Av,Adv);

// Define Bv
Bv = [
  -1.  -2.  -3.
  -2.  -5.   5.
  -1.   8.  -6.
];

//
// Relative and absolute tolerances.
//
rtol = 1.e-8;
atol = 1.e-8;

// TODO :  ".\"

for op = ["+" "-" "*" "/" "\" ".*" "./" ".*."]
    checkOp(op,DIFFA,Bv,rtol,atol);
end

for op = ["<" ">" "<=" ">=" "<>"]
    checkBooleanOp(op,DIFFA,Bv,rtol,atol);
end
//
// Unitary operators.
if ( %f ) then
for op = ["~"]
    mprintf("Checking %s\n",op);
    // op(DIFFA)
    // Compare with Av OP Bv
    mprintf("\t 1. OP DIFFA\n");
    instr1 = "C1 = " + op + " DIFFA";
    execstr(instr1);
    instr2 = "C2 = " + op + " Av";
    execstr(instr2);
    assert_checkequal(C1,C2);
end
end

// Define RDR (for pow)
Rv = 7.;

checkOp("^",DIFFA,Rv,rtol,atol);
