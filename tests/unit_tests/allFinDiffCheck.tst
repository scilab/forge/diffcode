// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// In this test, we check elementary functions.
//
// 1. We compute the function directly.
// 2. We compare with finite differences.


//
// Relative and absolute tolerances.
//
rtol = 1.e-6;
atol = 1.e-8;

// Check a function C = fun(a).
//
// Arguments
// fun: a function or a list.
// name: a 1-by-1 matrix of strings, the name of the function under test
// ADA: a TDFFCD, the current point.
// rtol: the relative tolerance
// atol: the absolute tolerance
//
// Description
// 1. We compute the function directly.
//    Compare with C.v
// 2. We compute with finite differences.
//    Compare with C.dv
function checkFunction(__fun__,name,ADA,rtol,atol)
    Av = ADA.v
    Adv = ADA.dv
    mprintf("Checking %s: ",name)
    //
    // 1. Check the value
    C1 = applyFunction(ADA,__fun__)
    C2 = applyFunction(Av,__fun__)
    assert_checkalmostequal(C1.v,C2,rtol,atol)
    //
    // 2. Check the derivative
    [m,n]=size(Av)
    C3 = numderivative(list(applyFunctionDerivative,__fun__,m,n),Av(:))
    dv = C3*(Adv(:))
    assert_checkalmostequal(C1.dv(:),dv,rtol,atol)
    mprintf("\t OK\n")
endfunction

// Check a function C = fun(a,b).
//
// Arguments
// fun: a function or a list.
// name: a 1-by-1 matrix of strings, the name of the function under test
// ADA: a TDFFCD, the first point.
// BDB: a TDFFCD, the second point.
// rtol: the relative tolerance
// atol: the absolute tolerance
//
// Description
// 1. We compute the function directly.
//    Compare with C.v
// 2. We compute with finite differences.
//    Compare with C.dv
function checkFunction2args(__fun__,name,ADA,BDB,rtol,atol)
    Av = ADA.v
    Adv = ADA.dv
    Bv = BDB.v
    Bdv = BDB.dv
    mprintf("Checking %s: ",name)
    //
    // 1. Check the value
    C1 = applyFunction2args(ADA,BDB,__fun__)
    C2 = applyFunction2args(Av,Bv,__fun__)
    if ( typeof(C1)=="boolean" ) then
        assert_checkequal(C1,C2)
    else
        assert_checkalmostequal(C1.v,C2,rtol,atol)
    end
    //
    // 2. Check the derivative
    if ( typeof(C1)<>"boolean" ) then
        [mA,nA]=size(Av)
        [mB,nB]=size(Bv)
        C3 = numderivative(list(applyFunDerivative2args,__fun__,mA,nA,mB,nB),[Av(:);Bv(:)])
        dv = C3*([Adv(:);Bdv(:)])
        assert_checkalmostequal(C1.dv(:),dv,rtol,atol)
    end
    mprintf("\t OK\n")
endfunction

// Compute fun(x)
// This is a workaround, because numderivative
// cannot derive compiled functions.
// Also, numderivative cannot derive matrix functions.
function y = applyFunction(x,__fun__)
    if ( typeof(__fun__) == "function" ) then
        y = __fun__ (x)
    else
        __fun__f__= __fun__(1)
        y = __fun__f__(x,__fun__(2:$))
    end
endfunction

// Compute fun(a,b)
// This is a workaround, because numderivative
// cannot derive compiled functions.
// Also, numderivative cannot derive matrix functions.
function y = applyFunction2args(a,b,__fun__)
    if ( typeof(__fun__) == "function" ) then
        y = __fun__ (a,b)
    else
        __fun__f__= __fun__(1)
        y = __fun__f__(a,b,__fun__(2:$))
    end
endfunction

// Compute fun(x)
// This is a workaround, because numderivative
// cannot derive compiled functions.
// Also, numderivative cannot derive matrix functions.
function y = applyFunctionDerivative(x,__fun__,m,n)
    x = matrix(x,m,n)
    y = applyFunction(x,__fun__)
    y = y(:)
endfunction

// Compute fun(a,b)
// This is a workaround, because numderivative
// cannot derive compiled functions.
// Also, numderivative cannot derive matrix functions.
function y = applyFunDerivative2args(x,__fun__,mA,nA,mB,nB)
    a = matrix(x(1:mA*nA),mA,nA)
    b = matrix(x(mA*nA+1:mA*nA+mB*nB),mB,nB)
    y = applyFunction2args(a,b,__fun__)
    y = y(:)
endfunction

// Compute fun(x)
// This is a workaround, because numderivative
// cannot derive compiled functions.
// Also, numderivative cannot derive matrix functions.
function y = applySimpleStringFun(x,fun)
    instr = "y = " + fun + "(x)"
    execstr(instr)
endfunction

// Compute op(a,b)
// This is a workaround, because numderivative
// cannot derive compiled functions.
// Also, numderivative cannot derive matrix functions.
function y = applyStringOpDerivative(x,op,mA,nA,mB,nB)
    a = matrix(x(1:mA*nA),mA,nA)
    b = matrix(x(mA*nA+1:mA*nA+mB*nB),mB,nB)
    instr = "y = a " + op + " b"
    execstr(instr)
    y = y(:)
endfunction

// Compute op(a,b)
// This is a workaround, because numderivative
// cannot derive compiled functions.
// Also, numderivative cannot derive matrix functions.
function y = applyStringOp(ADA,BDB,op)
    instr = "y = ADA " + op + " BDB"
    execstr(instr);
endfunction

function y = applyQuote(A)
  y = A'
endfunction

function y = applyElementwiseQuote(A)
  y = A.'
endfunction

////////////////////////////////////////////////////////////////

// Define ADA
Av = [
-5.    7.    3.
-7.   -1.    2.
];
Adv = [
-4.   6.   -7.
2.  -6.    4.
];
ADA=diffcode_der(Av,Adv);

for fun = ["sin" "cos" "tan" "exp" "log" "sqrt"]
    checkFunction(list(applySimpleStringFun,fun),fun,ADA,rtol,atol);
end

////////////////////////////////////////////////////////////////

// Code for inverse trigo functions

// Define ADA
Av = [
-0.9076572  -0.8057364    0.6469156
-0.7017720   0.9881370    0.6438065
];
Adv = [
-4.   6.   -7.
2.  -6.    4.
];
ADA=diffcode_der(Av,Adv);

for fun = ["asin" "acos" "atan"]
    checkFunction(list(applySimpleStringFun,fun),fun,ADA,rtol,atol);
end

////////////////////////////////////////////////////////////////

// Code for inverse hyperbolic trigo functions
// Cannot work.
// See bug : http://bugzilla.scilab.org/show_bug.cgi?id=10111
// "We cannot overload functions"

// Define ADA
Av = [
1.9133759    1.6323592    1.0975404
1.221034     1.3081671    1.5472206
];
Adv = [
-4.   6.   -7.
2.  -6.    4.
];
ADA=diffcode_der(Av,Adv);

if ( %f ) then
    for fun = ["acosh" "asinh" "atanh" "cotg"]
        checkFunction(list(applySimpleStringFun,fun),fun,ADA,rtol,atol);
    end
end

////////////////////////////////////////////////////////////////
// Code for matrix functions.
// Square functions.
// Define ADA
// "trace" cannot be overloaded: it is a macro.
Av = [
-5.   7.   3.
-7.  -1.   2.
-4.   3.  -7.
];
Adv = [
-4.   6.  -7.
2.  -6.   0.
9.  -5.   4.
];
ADA=diffcode_der(Av,Adv);

for fun = ["det"]
    checkFunction(list(applySimpleStringFun,fun),fun,ADA,rtol,atol);
end
if ( %f ) then
    for fun = ["trace"]
        checkFunction(list(applySimpleStringFun,fun),fun,ADA,rtol,atol);
    end
end

////////////////////////////////////////////////////////////////
// Code for operators.

// Define ADA
Av = [
-5.    7.    3.
-7.   -1.    2.
];
Adv = [
-4.   6.   -7.
2.  -6.    4.
];
ADA=diffcode_der(Av,Adv);

// Define BDB
Bv = [
-1.  -2.  -3.
-2.  -5.   5.
];
Bdv = [
-1.  -1.  -5.
-2.  -9.   5.
];
BDB=diffcode_der(Bv,Bdv);

// Define CDC (for multiplication)
Cv = [
-1.  -3.   7.
-2.  -5.  -5.
-2.   5.   8.
];
Cdv = [
-1.  -1.  -3.
-2.  -9.  -1.
-5.   5.   1.
];
CDC=diffcode_der(Cv,Cdv);


for fun = ["+" "-" ".*" "./" ".*."]
    checkFunction2args(list(applyStringOp,fun),fun,ADA,BDB,rtol,atol);
end
checkFunction2args(list(applyStringOp,"*"),"*",ADA,CDC,rtol,atol);


// For Boolean operators

for fun = ["<" ">" "<=" ">="]
    checkFunction2args(list(applyStringOp,fun),fun,ADA,BDB,rtol,atol);
end

// For pow
// Define SDS (for pow)
Sv = 3.;
Sdv = -1.;
SDS=diffcode_der(Sv,Sdv);

// Define TDT (for pow)
Tv = 7.;
Tdv = -9.;
TDT=diffcode_der(Tv,Tdv);

// Define UDU (for pow) - square
Uv = [
-1.  -3.
-2.  -1.
];
Uv = expm(Uv);
Udv = [
-1.  -1.
-2.  -9.
];
UDU=diffcode_der(Uv,Udv);

// Define RDR (for pow)
// TODO : fix the derivative when Rdv is nonzero.
Rv = 7.;
Rdv = 0.;
RDR=diffcode_der(Rv,Rdv);

// Define VDV (for pow) - rectangular
Vv = [
-1.  -3. 3.5
-2.  -1. 2.4
];
Vdv = [
 2.4  -1.  -1.
-2.   3.5  -9.
];
VDV=diffcode_der(Vv,Vdv);

// Define WDW (for pow) - row vector
Wv = [
-1.  -3. 3.5
];
Wdv = [
 2.4  -1.  -1.
];
WDW=diffcode_der(Wv,Wdv);

// Define ZDZ (for pow) - scalar with fractionnal part
Zv = [
-1.5
];
Zdv = [
 2.4
];
ZDZ=diffcode_der(Zv,Zdv);

// Define HDH (for pow) - negative integer scalar
Hv = [
-3.
];
Hdv = [
 0.
];
HDH=diffcode_der(Hv,Hdv);

checkFunction2args(list(applyStringOp,"^"),"^ (scalar^scalar)",TDT,SDS,rtol,atol);
checkFunction2args(list(applyStringOp,"^"),"^ (square matrix^+scalar)",UDU,RDR,rtol,atol);
checkFunction2args(list(applyStringOp,"^"),"^ (square matrix^-scalar)",UDU,HDH,rtol,atol);
checkFunction2args(list(applyStringOp,"^"),"^ (scalar^square matrix)",RDR,UDU,rtol,atol);
checkFunction2args(list(applyStringOp,"^"),"^ (scalar^rectangular matrix)",RDR,VDV,rtol,atol);
checkFunction2args(list(applyStringOp,"^"),"^ (row vector^scalar)",WDW,ZDZ,rtol,atol);

////////////////////////////////////////////////////////////////
// Code for quote.
checkFunction(applyQuote,"''",ADA,rtol,atol);
//checkFunction(applyElementwiseQuote,".''",ADA,rtol,atol);
