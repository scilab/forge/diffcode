// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

// DIFF * DIFF
x=0;
xdx=diffcode_der(x,1);
A=[1 1 0.5;1 xdx xdx;0.5 0.5 0.5];
C = A*A;
Ev = [
2.25 1.25 0.75
1 1 0.5
1.25 0.75 0.5
];
Edv = [
0 1 1
1.5 0.5 0.5
0 0.5 0.5
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
// DIFF * double
x=0;
xdx=diffcode_der(x,1);
A=[1 1 0.5;1 xdx xdx;0.5 0.5 0.5];
B=[3 2 1;0.5 2 -6;3 12 0.5];
C = A*B;
Ev = [
5 10 -4.75
3 2 1
3.25 8 -2.25
];
Edv = [
0 0 0
3.5 14 -5.5
0 0 0
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
// double * DIFF
x=0;
xdx=diffcode_der(x,1);
A=[1 1 0.5;1 xdx xdx;0.5 0.5 0.5];
B=[3 2 1;0.5 2 -6;3 12 0.5];
C = B*A;
Ev = [
5.5 3.5 2
-0.5 -2.5 -2.75
15.25 3.25 1.75
];
Edv = [
0 2 2
0 2 2
0 12 12
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
