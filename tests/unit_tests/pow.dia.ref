// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// <-- CLI SHELL MODE -->
//
// DIFF^3 - square matrix
x=0;
xdx=diffcode_der(x,1);
A=[1 1 -0.5;1 xdx xdx;0.5 0.5 0.5];
C = A^3;
Ev = [
2.125 1.375 -1.25
1.75 0.75 -0.75
2 1.25 -0.625
];
Edv = [
    1.5     1.25    1.25  
    2.25    2.75    0.5   
    0.75    1.      1.    
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
//
// DIFF.^3
x=2;
xdx=diffcode_der(x,1);
A=[1 1 -0.5;1 xdx xdx;0.5 0.5 0.5];
C = A.^3;
Ev = [
1 1 -0.125
1 8 8
0.125 0.125 0.125
];
Edv = [
0 0 0
0 12 12
0 0 0
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
//
// DIFF^3 - row vector (elementwise pow)
x=2;
xdx=diffcode_der(x,1);
A=[1 xdx xdx];
C = A^3;
Ev = [
    1.    8.    8.  
];
Edv = [
    0.    12.    12.  
];
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
//
// DIFF^3 - column vector (elementwise pow)
x=2;
xdx=diffcode_der(x,1);
A=[1 xdx xdx]';
C = A^3;
Ev = [
    1.    8.    8.  
]';
Edv = [
    0.    12.    12.  
]';
assert_checkequal(C.v,Ev);
assert_checkequal(C.dv,Edv);
//
// 3^DIFF - rectangular matrix
x=0;
xdx=diffcode_der(x,1);
A=[1 1 -0.5;1 xdx xdx];
C = 3^A;
Ev = [
3 3 0.5773503
3 1 1
];
Edv = [
0 0 0
0 1.0986123 1.0986123
];
assert_checkalmostequal(C.v,Ev,1.e-6);
assert_checkalmostequal(C.dv,Edv,1.e-6);
//
// ScalarDIFF^MatrixDIFF - rectangular matrix
ydy=diffcode_der(3,1);
x=0;
xdx=diffcode_der(x,1);
A=[1 1 -0.5;1 xdx xdx];
C = ydy^A;
Ev = [
3 3 0.5773503
3 1 1
];
Edv = [
1 1 -0.0962250
1 1.0986123 1.0986123
];
assert_checkalmostequal(C.v,Ev,1.e-6);
assert_checkalmostequal(C.dv,Edv,1.e-6);
