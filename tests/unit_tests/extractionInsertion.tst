// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// <-- CLI SHELL MODE -->

//
// Extraction xdx = ydy(i)
//
v = [
2.84168
4.04765
5.77972
8.27314
11.8707
];
dv = [
1.34985
3.64423
7.37880
13.2804
22.4084
];
x = diffcode_der(v,dv);
C = x(3);
assert_checkalmostequal(C.v,5.77972);
assert_checkalmostequal(C.dv,7.37880);
//
// Extraction xdx = ydy(i,j)
//
v = [
2.8416835 3.5753997 4.6699757 6.3028912 8.7389149
3.3139435 4.0476597 5.1422357 6.7751512 9.2111749
3.9514278 4.685144 5.77972 7.4126355 9.8486592
4.8119416 5.5456579 6.6402338 8.2731493 10.709173
5.9735138 6.70723 7.801806 9.4347215 11.870745
];
dv = [
1.3498588 1.3498588 1.3498588 1.3498588 1.3498588
3.6442376 3.6442376 3.6442376 3.6442376 3.6442376
7.3788093 7.3788093 7.3788093 7.3788093 7.3788093
13.280468 13.280468 13.280468 13.280468 13.280468
22.408445 22.408445 22.408445 22.408445 22.408445
];
x = diffcode_der(v,dv);
C = x(3,2);
assert_checkalmostequal(C.v,4.685144);
assert_checkalmostequal(C.dv,7.3788093);
//
// Extraction xdx = ydy(i1,i2,i3)
//
v = ones(4,3,2);
dv = ones(4,3,2);
x = diffcode_der(v,dv);
C = x(3,2,1);
assert_checkalmostequal(C.v,1);
assert_checkalmostequal(C.dv,1);
//
// Insertion ydy(i) = xdx
//
fdf = diffcode_der(1,2);
x = diffcode_der(0.3,0.4);
fdf(3) = x;
Ev = [
1
0
0.3
];
Edv = [
2
0
0.4
];
assert_checkalmostequal(Ev,fdf.v);
assert_checkalmostequal(Edv,fdf.dv);
//
// Insertion ydy(i) = x
//
fdf = diffcode_der(1,2);
x = 0.3;
fdf(3) = x;
Ev = [
1
0
0.3
];
Edv = [
2
0
0
];
assert_checkalmostequal(Ev,fdf.v);
assert_checkalmostequal(Edv,fdf.dv);
//
// Insertion ydy(i,j) = xdx
//
fdf = diffcode_der(1,2);
x = diffcode_der(0.3,0.4);
fdf(3,2) = x;
Ev = [
1 0
0 0
0 0.3
];
Edv = [
2 0
0 0
0 0.4
];
assert_checkalmostequal(Ev,fdf.v);
assert_checkalmostequal(Edv,fdf.dv);
//
// Insertion ydy(i1,i2,i3) = xdx
//
fdf = diffcode_der(1,2);
x = diffcode_der(0.3,0.4);
fdf(2,3,4) = x;
assert_checkequal(size(fdf),[2 3 4]);
//
// Insertion ydy(i,j) = x
//
fdf = diffcode_der(1,2);
x = 0.3;
fdf(3,2) = x;
Ev = [
1 0
0 0
0 0.3
];
Edv = [
2 0
0 0
0 0.
];
assert_checkalmostequal(Ev,fdf.v);
assert_checkalmostequal(Edv,fdf.dv);
//
// Insertion ydy(i1,i2,i3) = x
//
fdf = diffcode_der(1,2);
x = 0.3;
fdf(3,2,4) = x;
Ev = [
1 0
0 0
0 0.3
];
Edv = [
2 0
0 0
0 0.
];
assert_checkalmostequal(size(fdf.v),[3 2 4]);
assert_checkalmostequal(size(fdf.dv),[3 2 4]);
//
// Insertion ydy(i) = xdx (within a loop)
//
m = 10;
v = ones(m,1);
dv = ones(m,1);
fdf = diffcode_der(v,dv);
//
v= [0.3;0.4];
dv=[1;0];
x = diffcode_der(v,dv);
//
for i = 1:m
  fdf(i) = exp(i*x(1))+exp(i*x(2));
end
Ev = [
2.8416835
4.0476597
5.77972
8.2731493
11.870745
17.072824
24.610817
35.555707
51.477966
74.683687
];
Edv = [
1.3498588
3.6442376
7.3788093
13.280468
22.408445
36.297885
57.163189
88.185411
133.91759
200.85537
];
assert_checkalmostequal(Ev,fdf.v,1.e-7);
assert_checkalmostequal(Edv,fdf.dv,1.e-7);
//
// Insertion ydy(i,j) = xdx (within a loop)
//
fdf = [];
m = 5;
v = ones(m,m);
dv = ones(m,m);
fdf = diffcode_der(v,dv);
//
v= [0.3;0.4];
dv=[1;0];
x = diffcode_der(v,dv);
//
for i = 1:m
for j = 1:m
  fdf(i,j) = exp(i*x(1))+exp(j*x(2));
end
end
Ev = [
2.8416835 3.5753997 4.6699757 6.3028912 8.7389149
3.3139435 4.0476597 5.1422357 6.7751512 9.2111749
3.9514278 4.685144 5.77972 7.4126355 9.8486592
4.8119416 5.5456579 6.6402338 8.2731493 10.709173
5.9735138 6.70723 7.801806 9.4347215 11.870745
];
Edv = [
1.3498588 1.3498588 1.3498588 1.3498588 1.3498588
3.6442376 3.6442376 3.6442376 3.6442376 3.6442376
7.3788093 7.3788093 7.3788093 7.3788093 7.3788093
13.280468 13.280468 13.280468 13.280468 13.280468
22.408445 22.408445 22.408445 22.408445 22.408445
];
assert_checkalmostequal(Ev,fdf.v,1.e-7);
assert_checkalmostequal(Edv,fdf.dv,1.e-7);
