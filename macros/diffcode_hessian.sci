// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function H = diffcode_hessian(__diffcode_hessian_f__,x)
    // Compute the Hessian of the function.
    //
    // Calling Sequence
    //   H = diffcode_hessian(f,x)
    //
    // Parameters
    //   f : a function or a list, the function to differentiate.
    //   x : a n-by-1 matrix of doubles, real, the point where to compute the derivatives
    //   H : a n-ny-n-by-m matrix of doubles, the exact Hessian. The matrix <literal>H(:,:,k)</literal> is the Hessian of <literal>fk</literal>, for <literal>k=1,2,...,m</literal>.
    //
    // Description
    // Computes the exact Hessian matrix of the function.
    // The algorithm  uses an exact differentiation method.
    //
    // If <literal>f</literal> is a compiled function, then <literal>diffcode_hessian</literal>
    // cannot differentiate <literal>f</literal>.
    //
    // The function must have header
    //
    // <screen>
    // y = f( x )
    // </screen>
    //
    // where
    // <itemizedlist>
    // <listitem><para>x is a n-by-1 matrix of doubles,</para></listitem>
    // <listitem><para>y is a m-by-1 matrix of doubles.</para></listitem>
    // </itemizedlist>
    //
    // It might happen that the function requires additionnal
    // arguments to be evaluated.
    // In this case, we can use the following feature.
    // The function <literal>f</literal> can also be the list <literal>(func,a1,a2,...)</literal>
    // where, <literal>func</literal>, the first element in the list,
    // must be a function with header
    //
    // <screen>
    // y = func ( x , a1 , a2 , ... ).
    // </screen>
    //
    // In this case, the input arguments <literal>a1, a2, ...</literal> will be automatically be appended at the
    // end of the calling sequence of <literal>func</literal>.
    //
    // The <literal>diffcode_hessian</literal> function calls the function
    // <literal>f</literal> several times in order to compute its
    // derivatives.
    // The total function evaluations is <literal>n*(n+1)/2</literal> times, where <literal>n</literal>
    // is the size of <literal>x</literal>.
    //
    // Examples
    //  // The function to differentiate
    //  function y=f(x)
    //    f1 = sin(x(1)*x(2))+exp(x(2)*x(3)+x(1))
    //    f2 = sum(x.^3)
    //    y=[f1;f2]
    //  endfunction
    //  // The exact Hessian
    //  function H = exactH(x)
    //      H1(1,1) = -sin(x(1)*x(2))*x(2)^2+exp(x(2)*x(3)+x(1))
    //      H1(1,2) = cos(x(1)*x(2)) - sin(x(1)*x(2))*x(2)*x(1)+exp(x(2)*x(3)+x(1))*x(3)
    //      H1(1,3) = exp(x(2)*x(3)+x(1))*x(2)
    //      H1(2,1) = H1(1,2)
    //      H1(2,2) = -sin(x(1)*x(2))*x(1)^2+exp(x(2)*x(3)+x(1))*x(3)^2
    //      H1(2,3) = exp(x(2)*x(3)+x(1))+exp(x(2)*x(3)+x(1))*x(3)*x(2)
    //      H1(3,1) = H1(1,3)
    //      H1(3,2) = H1(2,3)
    //      H1(3,3) = exp(x(2)*x(3)+x(1))*x(2)^2
    //      //
    //      H2(1,1) = 6*x(1)
    //      H2(1,2) = 0
    //      H2(1,3) = 0
    //      H2(2,1) = H2(1,2)
    //      H2(2,2) = 6*x(2)
    //      H2(2,3) = 0
    //      H2(3,1) = H2(1,3)
    //      H2(3,2) = H2(2,3)
    //      H2(3,3) = 6*x(3)
    //      //
    //      H(:,:,1) = H1
    //      H(:,:,2) = H2
    //  endfunction
    // // Compute the exact Hessian
    // x=[1;2;3];
    // H=diffcode_hessian(f,x)
    // Hexact = exactH(x)
    // and(H==Hexact)
    //
    // // Passing extra parameters
    // function y=G(x,p)
    //   f1 = sin(x(1)*x(2)*p)+exp(x(2)*x(3)+x(1))
    //   f2 = sum(x.^3)
    //   y=[f1; f2]
    // endfunction
    // p=1;
    // h=1e-3;
    // H=diffcode_hessian(list(G,p),x)
    //
    // Authors
    // Michael Baudin, DIGITEO, 2011
    //

    //
    // Check input arguments
    [lhs,rhs]=argn();
    apifun_checkrhs ( "diffcode_hessian" , rhs , 2 )
    apifun_checklhs ( "diffcode_hessian" , lhs , 1 )
    //
    // Get input arguments
    //
    // Manage x, to get the size n.
    apifun_checktype ( "diffcode_hessian" , x , "x" , 1 , "constant" )
    [n,p] = size(x)
    apifun_checkvector ( "diffcode_hessian" , x , "x" , 1 )
    // Make x a column vector, if required
    if ( p<>1 ) then
        x = x(:)
        [n,p] = size(x)
    end
    //
    // Check types
    apifun_checktype ( "diffcode_hessian" , __diffcode_hessian_f__ , "f" , 2 , ["function" "list"])
    if type(__diffcode_hessian_f__)==15 then
        // List case
        // Check that the first element in the list is a function
        apifun_checktype ( "diffcode_hessian" , __diffcode_hessian_f__(1) , "f" , 2 , "function" )
    end
    //
    // Check sizes
    if type(__diffcode_hessian_f__)==15 then
        // List case
        if ( length(__diffcode_hessian_f__) < 2 ) then
            error(msprintf(gettext("%s: Wrong size for input argument #%d: %d-element list expected.\n"),"diffcode_hessian",2,2));
        end
    end
    //
    // Check value
    // Nothing to do.
    //
    // Proceed...
    dx = zeros(n,1)
    dy = zeros(n,1)
    for i = 1 : n
        dx(i)=1
        xdx = diffcode_der(x,dx)
        for j = 1 : n
            if ( j >= i ) then
                dy(j)=1
                ydy = diffcode_der(xdx,dy)
                funval=numderivative_evalf(__diffcode_hessian_f__,ydy)
                Hij = funval.dv.dv
                H(i,j,:) = Hij
                dy(j)=0
            end
            if ( j > i ) then
                H(j,i,:) = H(i,j,:)
            end
        end
        dx(i)=0
    end
endfunction
// numderivative_evalf --
// Computes the value of __numderivative_f__ at the point x.
// The argument __numderivative_f__ can be a function (macro or linked code) or a list.
function y=numderivative_evalf(__numderivative_f__,x)
    if type(__numderivative_f__)==15 then
        // List case
        __numderivative_fun__=__numderivative_f__(1);
        instr = "y=__numderivative_fun__(x,__numderivative_f__(2:$))"
    elseif or(type(__numderivative_f__)==[11 13]) then
        // Function case
        instr = "y=__numderivative_f__(x)"
    else
        error(msprintf(gettext("%s: Wrong type for input argument #%d: A function expected.\n"),"numderivative",1));
    end
    ierr=execstr(instr,"errcatch")
    if ierr <> 0 then
        lamsg = lasterror()
        lclmsg = "%s: Error while evaluating the function: ""%s""\n"
        error(msprintf(gettext(lclmsg),"numderivative",lamsg));
    end
endfunction
