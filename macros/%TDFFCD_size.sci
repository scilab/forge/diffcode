// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [m,n]=%TDFFCD_size(x,flag)
// Copyright INRIA
  if argn(1)==1
    if argn(2)==1 then
      m=size(x.v)
    else
      m=size(x.v,flag)
    end
  else 
    [m,n]=size(x.v)
  end;
endfunction
