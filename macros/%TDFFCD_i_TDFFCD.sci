// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function y = %TDFFCD_i_TDFFCD (varargin)
    // y = %TDFFCD_i_TDFFCD (i1,i2,...,ik,x,y)
    // y(i1,i2,...,ik) = x
    x = varargin($-1)
    y = varargin($)
    y.v(varargin(1:$-2)) = x.v
    y.dv(varargin(1:$-2)) = x.dv
endfunction
