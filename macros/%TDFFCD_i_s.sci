// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2011 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function y = %TDFFCD_i_s (varargin)
    // y = %TDFFCD_i_s (i1,i2,...,ik,x,y)
    // y(i1,i2,...,ik) = x
    x = varargin($-1)
    y = varargin($)
    y = diffcode_der(y,zeros(y))
    y(varargin(1:$-2)) = x
endfunction
