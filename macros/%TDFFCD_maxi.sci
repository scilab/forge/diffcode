// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2009 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [m,k]=%TDFFCD_maxi(varargin)
  nv=size(varargin)
  if nv==2&type(varargin(2))==10 then //max(A,orient)
    arg=varargin(1);orient=varargin(2);
    if orient=='m' then
      d=find(size(arg)>1,1)
      if d==1 then
	orient='r'
      else
	orient='*'
      end
    end
    [m,k]=max(arg.v,orient)
    select orient
    case 'r'
      m=arg(k(1),:)
    case 'c'
      m=arg(:,k(1))
    case '*'
      m=arg(k(1),k(2))
    end
  elseif nv==1 then
    arg=varargin(1)
    if type(arg)==15 then //max(list(A1,A2,...,An))
    else  //max(A)
      [m,k]=max(arg.v)
      m=arg(k(1),k(2))
    end
  else //max(A1,A2,...,An)
    T=varargin(1).v;k=ones(T);m=varargin(1)
    for i=2:size(varargin)
      [w,kk]=max(T,varargin(k).v)
      k(kk==2)=i
      m(kk==2)=varargin(k)(kk==2)
    end
  end
endfunction
