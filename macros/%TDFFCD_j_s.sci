// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z=%TDFFCD_j_s (x,y)
  v = x.v
  dv = x.dv
  z = diffcode_der(v.^y,y.*v.^(y-1).*dv)
endfunction
