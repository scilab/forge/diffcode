// Copyright (C) 2012 - Michael Baudin
// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function [f,g,ind]=diffcode_CDcost(x,ind,fun,varargin)
    //external for optim
    //computes gradient using Code differentiation
    if argn(2)<4 then
        varargin=list()
    end
    f=fun(x,varargin(:))
    g=zeros(x)
    dx=zeros(x);
    for k=1:size(x,"*")
        dx(k)=1
        F=fun(diffcode_der(x,dx),varargin(:))
        g(k)=F.dv
        dx(k)=0
    end
endfunction
