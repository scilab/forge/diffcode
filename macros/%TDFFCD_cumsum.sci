// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2009 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function x=%TDFFCD_cumsum(x,job)
  if argn(2)==1 then
    x.v=cumsum(x.v)
    x.dv=cumsum(x.dv)
  else
    x.v=cumsum(x.v,job)
    x.dv=cumsum(x.dv,job)
  end
endfunction
