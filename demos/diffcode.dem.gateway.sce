// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
demopath = get_absolute_file_path("diffcode.dem.gateway.sce");
subdemolist = [
"demo1", "demo1.sce"; ..
"demo2", "demo2.sce"; ..
"demo3", "demo3.sce"; ..
"demo4", "demo4.sce"; ..
"demo5", "demo5.sce"; ..
"demo6", "demo6.sce"; ..
];
subdemolist(:,2) = demopath + subdemolist(:,2)
