// Copyright (C) 2011 - DIGITEO - Michael Baudin
// Copyright (C) 2002 - INRIA - Xavier Jonsson
// Copyright (C) 2002 - INRIA - Serge Steer
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

t=(0:0.01:5*%pi)';
y=sin(diffcode_der(t,ones(t))^2);
scf();
plot(t,[y.v,y.dv])

//========= E N D === O F === D E M O =========//
//
// Load this script into the editor
//
filename = "demo6.sce";
dname = get_absolute_file_path(filename);
editor ( fullfile(dname,filename) );
